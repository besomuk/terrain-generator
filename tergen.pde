int Y_AXIS = 1;
int X_AXIS = 2;

int horizontLine = 200;

color skyColor           = color(2,161, 232);
color landScapeColor     = color(214,0,150);
color landScapeFillColor = color(0,0,0); 

void setup() 
{
  size(600, 700);
  background(0);

  DrawSky();
  DrawLandscape();

}

void DrawSky()
{
  color b1 = color(0);
  color b2 = skyColor;
  
  setGradient(0, 0, width, horizontLine, b1, b2, Y_AXIS);
  //setGradient(width/2, 0, width/2, height, b2, b1, X_AXIS);
}

void DrawLandscape()
{
  int step = 1;
  int start = horizontLine;

  while (start < height)
  {
    start = start + step;
    MakeLandscape(start);
    step ++;
  }
}

void MakeLandscape(int y)
{
  //noFill();
  //color(landScapeColor);
  fill(landScapeFillColor);
  stroke(landScapeColor);
  smooth();
  
  /*
  beginShape();
  curveVertex(84,  91);
  curveVertex(84,  91);
  curveVertex(68,  19);
  curveVertex(21,  17);
  curveVertex(32, 100);
  curveVertex(32, 100);
  endShape();
  */
  
  //int y = height - 50;
  int step = 10;
 
  beginShape();
  
  curveVertex(0,y);
  
  for (int i = 0; i<=width; i+=step)
  {
    int faktor = 0;
    if ( i <= 50 || i >= width - 50)
      faktor = 0;
    
    //TODO
    else if ( i <= 100 || i >= width - 100)
      faktor = int(random (0,5));
      
    else if ( i <= 150 || i >= width - 150)
      faktor = int(random (0,10));
    
    else if ( i <= 200 || i >= width - 250)
      faktor = int(random (0,15));
      
    else if ( i <= 250 || i >= width - 250)
      faktor = int(random (0,20));  
      
    else if ( i <= 300 || i >= width - 300)
      faktor = int(random (0,25));
      
    else if ( i <= 350 || i >= width - 350)
      faktor = int(random (0,30));
      
    else 
      faktor = int(random (0,40)); 
      
    curveVertex(i, y - faktor);
  }
  
  curveVertex(width,y);
  
  endShape();
}

//https://processing.org/examples/lineargradient.html
void setGradient(int x, int y, float w, float h, color c1, color c2, int axis ) 
{
  noFill();

  if (axis == Y_AXIS)
  {  // Top to bottom gradient
    for (int i = y; i <= y+h; i++)
    {
      float inter = map(i, y, y+h, 0, 1);
      color c = lerpColor(c1, c2, inter);
      stroke(c);
      line(x, i, x+w, i);
    }
  }  
  else if (axis == X_AXIS)
  {  // Left to right gradient
    for (int i = x; i <= x+w; i++)
    {
      float inter = map(i, x, x+w, 0, 1);
      color c = lerpColor(c1, c2, inter);
      stroke(c);
      line(i, y, i, y+h);
    }
  }
}
